﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRPhysicalButton : MonoBehaviour {

	public UnityEngine.Events.UnityEvent action;

	public void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == GameData.vrHandTag) {
			action.Invoke();
		}
	}
}
