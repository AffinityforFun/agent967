﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorCollisionTracker : MonoBehaviour {

	public List<GameObject> objInRange = new List<GameObject>();

	public void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == GameData.objectTag)
			objInRange.Remove(other.gameObject);
	}

	public void OnTriggerExit(Collider other) {
		objInRange.Remove(other.gameObject);
	}
}
