﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disposal : MonoBehaviour {
	public DisposalObject scriptableObject;

	public void ReturnEvidence(Evidence evidence) {
		int evidenceID = evidence.scriptableObject.UID;
		int disposalID = scriptableObject.UID;
		int? symbolID = (evidence.optionalSymbol != null) ? (int?)evidence.optionalSymbol.UID : null;
		GameManager.instance.ReportEvidenceDistruction(evidenceID, disposalID, symbolID);
		EvidencePooler.ReturnObject(evidence.scriptableObject.UID, evidence.gameObject);
	}
}
